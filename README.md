# DDoS Tester
DDoS tester is used to test resiliency of networks against cyber security attacks. This program should not be used on networks that you do not control as this activity can cause sever problems. The goal of this software is to create a pluggable testing platform with a simple script for remote execution of DDoS command and a GUI frontend within Cockpit.

## Description
This project hping3. It is designed to allow for background attacks to persist given that hping3 program can disrupt typical command and control. It is not meant to replace hping3 but to allow testing of sequential attacks by prohibiting multiple execution. It tests for a running version and halts multiple iterations. It also contains a stop function which allows for attacks to be cancelled remotely.

## Installation
The target/client machine is the machine which performs the attack (ie. the attacker). This is usually an intermediary machine close to the victim. 

Copy hping3_attack.py in the files folder to the ssh home directory of the target machine. The target machine should have hping3 installed locally. If using Kali Linux, this is likely installed by default. The target machine must also have python available in /usr/bin/python.

## Command and Control Usage
The following lines of code should be implemented with a centralized command and control structure which provides an overall platform for Cybersecurity testing.

You will need a IPaddress/hostname for the client and the victim. You should specify a count and any additional [hping3 options](https://linux.die.net/man/8/hping3) you wish to pass.

For an ICMP attack
```ssh -o StrictHostKeyChecking=no ${client} ./hping3_attack.py start icmp  ${victim} -c ${count} ${options} &```

For an SYN attack
```ssh -o StrictHostKeyChecking=no ${client} ./hping3_attack.py start syn  ${victim} -c ${count} ${options} &```

For an UDP attack
```ssh -o StrictHostKeyChecking=no ${client} ./hping3_attack.py start udp  ${victim} -c ${count} ${options} &```

To stop an attack
```ssh -o StrictHostKeyChecking=no ${client} ./hping3_attack.py stop &```

### Optional Cockpit (Demo only)
You may also install the cockpit version which provides a simple, standalone implementation of command and control. Implement the base code of this repo as a cockpit module, removing the files folder.

## Acknowledgment
I wish to thank the Center for Cyber Security Research at the University of North Dakota for inspiring this application.

## License
This software is licensed under AGPL v3

